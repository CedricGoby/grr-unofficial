# Qu'est-ce que GRR ?

[GRR](https://github.com/JeromeDevome/GRR) est un système de Gestion et de Réservations de Ressources.

GRR est particulièrement adapté à la gestion et la réservation de salles et de matériels, mais il peut également être utilisé comme mini-agenda partagé.

# Tags et liens `Dockerfile`

-	[`4.3.2-php8.1-apache`](https://gitlab.com/CedricGoby/grr-unofficial/-/blob/a908de18c578edb090d9a0d958aa80e981437efb/4.3.2/php8.1/apache/Dockerfile)


# Comment utiliser cette image

```console
$ docker run --name mon-grr -p 80:80 -d cedricgoby/grr-unofficial:4.3.2-php8.1-apache
```

Vous pouvez alors accéder à grr dans un navigateur en utilisant l'adresse `http://localhost`.

## ... via [`docker-compose`](https://github.com/docker/compose)

Exemple `docker-compose.yml` pour `grr`:

```yaml
version: '3.8'

services:

  wordpress:
    image: cedricgoby/grr-unofficial:4.3.2-php8.1-apache
    restart: always
    ports:
      - 80:80
    volumes:
      - grr:/var/www/html

  db:
    image: mysql:8.1
    restart: always
    environment:
      MYSQL_DATABASE: exampledb
      MYSQL_USER: exampleuser
      MYSQL_PASSWORD: examplepass
      MYSQLROOT_PASSWORD: '1'
    volumes:
      - db:/var/lib/mysql

volumes:
  grr:
  db:
```

Exécutez `docker-compose up -d` et visitez `http://localhost`.

# License

Afficher les [informations de licence](https://github.com/JeromeDevome/GRR/blob/master/LICENSE) pour le logiciel contenu dans cette image.

Comme pour toutes les images Docker, celles-ci contiennent probablement également d'autres logiciels qui peuvent être soumis à d'autres licences (comme Bash, etc. de la distribution de base, ainsi que toutes les dépendances directes ou indirectes du logiciel principal contenu).

En ce qui concerne l'utilisation de toute image préconstruite, il incombe à l'utilisateur de l'image de s'assurer que toute utilisation de cette image est conforme aux licences pertinentes pour tout le logiciel# grr-unofficial
